import React from 'react';

class Planner {
    constructor() {
        this.actions = [];
        this.shouldRender = true;
        this.inProgress = false;
        this.stateChanged = false;
    }
    
    onRender(cb) {
        this.render = cb;
    }
    
    addAction(action) {
        this.actions.push(action);
    }
    
    notifyAboutStateChange(err) {
        if (err) {
            console.error(err);
            return;
        }
        if (this.inProgress) {
            this.stateChanged = true;
            return;
        }
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
            delete this.timeoutId;
        }
        this.shouldRender = true;
        this.runIt();
    }

    runIt() {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId);
            delete this.timeoutId;
        }
        this.inProgress = true;
        this.stateChanged = false;
        this.actions.forEach(action => {
            try {
                action();
            }
            catch (err) {
                console.error(err);
            }
        });
        this.inProgress = false;
        if (this.stateChanged) {
            this.shouldRender = true;
            this.runIt();
            return;
        }
        if (this.shouldRender) {
            try {
                this.render();
            }
            catch (err) {
                console.error(err);
            }
            this.shouldRender = false;
        }
        this.timeoutId = setTimeout(() => this.runIt(), 1000);
    }
}

var planner = new Planner();

export default planner;

export function notify(err) {
    return planner.notifyAboutStateChange(err);
}
