import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import App from './containers/App'
import planner from './planner'
import state from './state'
import * as actions from './actions';

for (var actionName in actions) {
    let action = actions[actionName];
    if (action.length === 0) {
        planner.addAction(action);
    }
}

planner.onRender(function() {
    render(
        <App {...state} />,
        document.getElementById('root')
    );
});

planner.runIt();
