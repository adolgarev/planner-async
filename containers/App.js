import React, { Component, PropTypes } from 'react'
import Picker from '../components/Picker'
import Posts from '../components/Posts'
import {selectSubreddit, invalidateReddit} from '../actions'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleRefreshClick = this.handleRefreshClick.bind(this);
    }
    
    handleChange(nextReddit) {
        selectSubreddit(nextReddit);
    }

    handleRefreshClick(e) {
        e.preventDefault();
        invalidateReddit(this.props.selectedSubreddit);
    }

    render() {
        const { selectedSubreddit, postsBySubreddit } = this.props;
        const { items: posts, isFetching, lastUpdated } = postsBySubreddit[selectedSubreddit];
        const isEmpty = posts.length === 0;
        return (
            <div>
            <Picker value={selectedSubreddit}
                    onChange={this.handleChange}
                    options={[ 'reactjs', 'frontend' ]} />
            <p>
                {lastUpdated &&
                <span>
                    Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
                    {' '}
                </span>
                }
                {!isFetching &&
                <a href="#"
                    onClick={this.handleRefreshClick}>
                    Refresh
                </a>
                }
            </p>
            {isEmpty
                ? (isFetching ? <h2>Loading...</h2> : <h2>Empty.</h2>)
                : <div style={{ opacity: isFetching ? 0.5 : 1 }}>
                    <Posts posts={posts} />
                </div>
            }
            </div>
        )
    }
}

App.propTypes = {
    selectedSubreddit: PropTypes.string.isRequired,
    postsBySubreddit: PropTypes.object.isRequired
}

