import fetch from 'isomorphic-fetch'
import s from '../state'
import {notify} from '../planner'

export function selectDefaultSubreddit() {
    if (s.selectedSubreddit !== null)
        return;
    s.selectedSubreddit = 'reactjs';
    notify();
}

export function fetchSelectedSubreddit() {
    if (s.selectedSubreddit === null)
        return;
    var selectedSubreddit = s.postsBySubreddit[s.selectedSubreddit];
    if (selectedSubreddit !== undefined
            && (selectedSubreddit.isFetching || selectedSubreddit.lastUpdated)
            && !selectedSubreddit.didInvalidate)
        return;
    var reddit = s.selectedSubreddit;
    if (s.postsBySubreddit[reddit] === undefined) {
        s.postsBySubreddit[reddit] = {
            items: []
        };
    }
    s.postsBySubreddit[reddit].isFetching = true;
    s.postsBySubreddit[reddit].didInvalidate = false;
    fetch(`https://www.reddit.com/r/${reddit}.json`)
        .then(response => response.json())
        .then(json => {
            let posts = json.data.children.map(child => child.data);
            s.postsBySubreddit[reddit].items = posts;
            s.postsBySubreddit[reddit].isFetching = false;
            s.postsBySubreddit[reddit].lastUpdated = new Date().getTime();
            notify();
        });
    notify();
}

export function selectSubreddit(reddit) {
    s.selectedSubreddit = reddit;
    notify();
}

export function invalidateReddit(reddit) {
    if (s.selectedSubreddit === null)
        return;
    var selectedSubreddit = s.postsBySubreddit[s.selectedSubreddit];
    if (selectedSubreddit !== undefined
            && (selectedSubreddit.isFetching || selectedSubreddit.didInvalidate))
        return;
    selectedSubreddit.didInvalidate = true;
    notify();
}

export function automaticallyRefreshSelectedSubredditEvery30Seconds() {
    if (s.selectedSubreddit === null)
        return;
    var selectedSubreddit = s.postsBySubreddit[s.selectedSubreddit];
    if (selectedSubreddit !== undefined
            && (selectedSubreddit.isFetching || selectedSubreddit.lastUpdated + 30000 > new Date().getTime()))
        return;
    selectedSubreddit.didInvalidate = true;
    notify();
}
